import itertools
from math import sqrt
from random import randrange
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import sm as sm
import statsmodels
from matplotlib import pyplot
from pandas import read_csv, DataFrame
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.seasonal import seasonal_decompose
from dateutil.parser import parse
from pandas.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error

#jdataset=pd.read_csv('jo1.csv')
#print(jdataset.shape)
#print(jdataset.head())
#print(jdataset.groupby('Year1').size())

#plt.hist(jdataset["Year1"])
#plt.show()
from statsmodels.tsa.statespace.sarimax import SARIMAX


#sb.scatterplot(x=jdataset["Year1"], y=jdataset["Average_Temperature"], data=jdataset, hue="mWord1")
#plt.show()

df = pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date')
df.head()

def plot_df(df, x, y, title="", xlabel='Year-Month', ylabel='Active Power', dpi=100):
    print("inside function")
    plt.figure(figsize=(15,10), dpi=dpi)
    plt.plot(x, y, color='tab:red')
    plt.gca().set(title=title, xlabel=xlabel, ylabel=ylabel)
    plt.show()

print("calling function")
plot_df(df, x=df.index, y=df.values, title='Monthly anti-diabetic drug sales in Australia from 1992 to 2008.')

fig, axes = plt.subplots(1,3, figsize=(20,4), dpi=100)
pd.read_csv('daily_trend.csv', parse_dates=['year'], index_col='year').plot(title='Trend Only', legend=False, ax=axes[0])
pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date').plot(title='Seasonality Only', legend=False, ax=axes[1])
pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date').plot(title='Trend and Seasonality', legend=False, ax=axes[2])

plt.show()

# Import Data
df = pd.read_csv('dayMonth.csv', parse_dates=['date'], index_col='date')
df.reset_index(inplace=True)

# Prepare data
df['year'] = [d.year for d in df.date]
df['month'] = [d.strftime('%b') for d in df.date]
years = df['year'].unique()

# Prep Colors
np.random.seed(100)

# Draw Plot
plt.figure(figsize=(15,10), dpi= 80)
for i, y in enumerate(years):
    if i > 0:
        plt.plot('month', 'value', data=df.loc[df.year==y, :], label=y)
        plt.text(df.loc[df.year==y, :].shape[0]-.9, df.loc[df.year==y, 'value'][-1:].values[0], y, fontsize=12)

# Decoration
plt.gca().set(xlim=(-0.3, 4), ylim=(0, 5), ylabel='$Drug Sales$', xlabel='$Month$')
plt.yticks(fontsize=12, alpha=.7)
plt.title("Seasonal Plot of Drug Sales Time Series", fontsize=20)
plt.show()

df = pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date')
df.reset_index(inplace=True)
df.isnull().sum()

print(df['value'].min(), df['value'].max())

#df = df.groupby('Date')
#print(df['value'].min(), df['value'].max())
#print(df['Date'])
#y = df['Date'].resample('M')

from statsmodels.tsa.seasonal import seasonal_decompose
from dateutil.parser import parse

# Import Data
df = pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date')
#df = pd.read_csv('month_year.csv', parse_dates=['month'], index_col='month')

# Multiplicative Decomposition
result_mul = seasonal_decompose(df['value'], model='multiplicative', extrapolate_trend='freq', freq=365)

# Additive Decomposition
result_add = seasonal_decompose(df['value'], model='additive', extrapolate_trend='freq',freq=365)

# Plot
plt.rcParams.update({'figure.figsize': (15,8)})
result_mul.plot().suptitle('Multiplicative Decompose', fontsize=22)
result_add.plot().suptitle('Additive Decomposition of Time-Series', fontsize=22)
plt.show()


series = read_csv('avg.csv', header=0, parse_dates=['Date'], index_col='Date')
#autocorrelation_plot(series)
#pyplot.show()

X = series.values
size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()
for t in range(len(test)):
	model = ARIMA(history, order=(1,1,1))
	model_fit = model.fit(disp=0)
	output = model_fit.forecast()
	predict = output[0]
	predictions.append(predict)
	expect = test[t]
	history.append(expect)
	print('predicted value=%f and expected value=%f' % (predict, expect))
print()
print()
error = mean_squared_error(test, predictions)
print('Observed Mean Squared Error : %.3f' % error)
# plot
pyplot.plot(test)
pyplot.plot(predictions, color='red')
plt.gca().set(title='Expected V/S Predicted Electricity Consumption', xlabel='Date', ylabel='Active Power Consumption')
pyplot.show()


# fit model
model = ARIMA(series, order=(1,1,1))
model_fit = model.fit(disp=0)
print(model_fit.summary())
# plot residual errors
residuals = DataFrame(model_fit.resid)
residuals.plot()
pyplot.show()
residuals.plot(kind='kde')
pyplot.show()
print(residuals.describe())

series = read_csv('avg.csv', header=0, parse_dates=['Date'], index_col='Date')
p = range(0, 2)
d = range(0, 2)
q = range(0, 2)
pdq = list(itertools.product(p, d, q))
seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

for param in pdq:
    for param_seasonal in seasonal_pdq:
        try:
            mod = SARIMAX(series, order=param,seasonal_order=param_seasonal, enforce_stationarity=False, enforce_invertibility=False)
            results = mod.fit()
            print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
        except:
            continue

mod = SARIMAX(series,
              order=(1, 1, 1),
              seasonal_order=(1, 0, 1, 12),
              enforce_stationarity=False,
              enforce_invertibility=False)
results = mod.fit()
results.plot_diagnostics(figsize=(15, 10))
plt.show()


pred = results.get_prediction(start=pd.to_datetime('2017-01-01'), dynamic=False)
pred_ci = pred.conf_int()
ax = y['2014':].plot(label='observed')
pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7, figsize=(14, 7))
ax.fill_between(pred_ci.index,
                pred_ci.iloc[:, 0],
                pred_ci.iloc[:, 1], color='k', alpha=.2)
ax.set_xlabel('Date')
ax.set_ylabel('Furniture Sales')
plt.legend()
plt.show()

df = pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date')
tr_start,tr_end = '2006-12-16','2009-07-21'
te_start,te_end = '2009-07-22','2010-11-26'
tra = df['value'][tr_start:tr_end].dropna()
tes = df['value'][te_start:te_end].dropna()


sarima = SARIMAX(tra, order=(1,1,1),seasonal_order=(1, 0, 1, 12),
                                enforce_stationarity=False, enforce_invertibility=False)
results = sarima.fit()
results.summary()

res = results.resid
print(tes)
pred = results.get_prediction(start=pd.to_datetime(tr_end), dynamic=False)
pred_ci=pred.conf_int()
ax = df['value'][tr_start:].plot(label='observed')
pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7, figsize=(14, 7))

ax.set_xlabel('Date')
ax.set_ylabel('Furniture Sales')
plt.legend()
plt.show()

y_forecasted = pred.predicted_mean
y_truth = df['value'][tr_start:]
mse = ((y_forecasted - y_truth) ** 2).mean()
print('The Mean Squared Error of our forecasts is {}'.format(round(mse, 2)))

#print('SARIMA model MSE:{}'.format(mean_squared_error(tes,pred)))




df = pd.read_csv('avg.csv', parse_dates=['Date'], index_col='Date')
tr_start,tr_end = '2006-12-16','2009-07-21'
te_start,te_end = '2009-07-22','2010-11-26'
tra = df['value'][tr_start:tr_end].dropna()
tes = df['value'][te_start:te_end].dropna()


sarima = SARIMAX(tra, order=(1,1,1),seasonal_order=(1, 0, 1, 12),
                                enforce_stationarity=False, enforce_invertibility=False)
results = sarima.fit()
results.summary()

res = results.resid
pred = results.get_prediction(start=pd.to_datetime(tr_end), dynamic=False)
pred_ci=pred.conf_int()
print()
print()
y_forecasted = pred.predicted_mean
y_truth = df['value'][tr_start:]
mse = ((y_forecasted - y_truth) ** 2).mean()
print('Observed Mean Squared Error by SARIMA Model Fit : {}'.format(round(mse, 3)))



